[ott-vscode](https://marketplace.visualstudio.com/items?itemName=JoeyEremondi.ott): an editor plugin to support syntax highlighting and inline error reporting for the [Ott Semantics Tool](http://www.cl.cam.ac.uk/%7Epes20/ott/).

[Lambda Pi Plus](http://lambda-pi-plus.github.io/): a simple dependently-typed language that you can [try in your browser](http://lambda-pi-plus.github.io/).

[UBC Beamer Theme](https://github.com/JoeyEremondi/UBC-Metropolis-Beamer): a modern Beamer theme with the UBC logo and colors, based on [Metropolis](https://github.com/matze/mtheme).