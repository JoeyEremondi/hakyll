--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend, (<>))
import           Hakyll hiding (defaultContext)
import qualified Hakyll
import Hakyll.Web.Pandoc.Biblio
import Hakyll.Web.Template hiding (defaultContext)
import Hakyll.Web.Template.List 
import Hakyll.Core.Identifier 
import Hakyll.Core.Item 
import Control.Monad (forM)
import Debug.Trace (trace)

import Data.Time.Clock
import Data.Time.Calendar
import StringUtils (replace)
import           Text.Jasmine
import qualified Data.ByteString.Lazy.Char8 as C

--------------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}



--Helpful stuff taken from https://github.com/jaspervdj/hakyll-bibtex/blob/master/site.hs
-- and https://github.com/mcmtroffaes/homepage/blob/master/posts/2015-01-09-hakyll-and-bibtex.markdown

main :: IO ()
main = do
    (year, _, _) <- getCurrentTime >>= return . toGregorian . utctDay
    let defaultContext = Hakyll.defaultContext <> constField "year" (show year)
    let postCtx :: Context String
        postCtx =
            dateField "date" "%B %e, %Y" <>
            defaultContext
    hakyllWith config $ do
        match "bib/*" $ compile biblioCompiler
        match "csl/*" $ compile cslCompiler
        match "images/*" $ do
            route   idRoute
            compile copyFileCompiler

        match "assets/css/*.css" $ do
            route   idRoute
            compile compressCssCompiler

            match "assets/css/main.scss" $ do
                route   idRoute
                compile compressScssCompiler

        match "assets/js/*" $ do
            route   idRoute
            compile compressJsCompiler
            
        match "assets/webfonts/*" $ do
                route   idRoute
                compile copyFileCompiler

        match "assets/fonts/*" $ do
            route   idRoute
            compile copyFileCompiler

            match "assets/img/*" $ do
                route   idRoute
                compile copyFileCompiler
    
            -- <text variable="URL" font-style="italic" prefix=". "/>
        match "pubs/*" $ do
            route   $ setExtension "html"
            compile $ do
                ourPath <- getResourceFilePath
                let bibPath = replace ".markdown" ".bib" $ replace "./pubs/" "bib/" $ ourPath
                pandocBiblioCompiler "csl/acm-sig-long.csl" bibPath
                    >>= saveSnapshot "biblio"

        create ["pubs.html"] $ do
            route  idRoute
            let pubsCtx =
                    constField "title" "Publications"            <>
                    defaultContext


            compile $ do
                pubTemp <- loadBody "templates/pubs.html"
                bibList <- loadAllSnapshots "pubs/*" "biblio"
                bibs <- trace ("Loaded " ++ (show $ length bibList) ++ " snapshots ") $ makeItem =<< applyTemplateList pubTemp defaultContext bibList
                loadAndApplyTemplate "templates/default.html" pubsCtx bibs -- Item (fromFilePath "pubs.html") bibs
                    >>= relativizeUrls


        match (fromList ["contact.markdown"]) $ do
            route   $ setExtension "html"
            compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls





        match "posts/*" $ do
            route $ setExtension "html"
            compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/post.html"    postCtx
                >>= loadAndApplyTemplate "templates/default.html" postCtx
                >>= relativizeUrls

        match "pages/*" $ do
            route $ setExtension "html"
            compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/default.html" postCtx
                >>= relativizeUrls

        create ["archive.html"] $ do
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll "posts/*"
                let archiveCtx =
                        listField "posts" postCtx (return posts) <>
                        constField "title" "Archives"            <>
                        defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                    >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                    >>= relativizeUrls


        create ["index.html"] $ do
            route idRoute
            compile $ do
                allPosts <- recentFirst =<< loadAll "posts/*"
                let posts = take 4 allPosts
                let archiveCtx =
                        listField "posts" postCtx (return posts) <>
                        constField "title" "About"            <>
                        constField "isHome" "True"            <>
                        defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/about.html" archiveCtx
                    >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                    >>= relativizeUrls


        match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------




-- | Create a JavaScript compiler that minifies the content
compressJsCompiler :: Compiler (Item String)
compressJsCompiler = do
  let minifyJS = C.unpack . minify . C.pack . itemBody
  s <- getResourceString
  return $ itemSetBody (minifyJS s) s


-- | Create a SCSS compiler that transpiles the SCSS to CSS and
-- minifies it (relying on the external 'sass' tool)
compressScssCompiler :: Compiler (Item String)
compressScssCompiler = do
  fmap (fmap compressCss) $
    getResourceString
    >>= withItemBody (unixFilter "sass" [ "-s"
                                        , "--scss"
                                        , "--compass"
                                        , "--style", "compressed"
                                        , "--load-path", "_scss"
                                        ])

