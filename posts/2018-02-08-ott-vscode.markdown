---
title: "ANN: Ott plugin for vscode"
---

I've made a package that integrates the [Ott Semantic Modeling tool](http://www.cl.cam.ac.uk/~pes20/ott/) into Visual Studio Code. 
Right now, it supports syntax highlighting, and showing error messages inline, but I hope to eventually add support for source formatting, and prettifying symbols.

You can check it out at [the Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=JoeyEremondi.ott). 
Pull requests, collaboration, suggestions are all welcome!

![Screenshot](https://raw.githubusercontent.com/JoeyEremondi/vscode-ott/master/media/screenshot.png )
